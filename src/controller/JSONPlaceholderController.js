const axios = require("axios")

module.exports = class JSONPlaceholderController { 
    static async getUsers(req,res){                    //requisição e resposta!
        try{
            const response = await axios.get(
                "https://jsonplaceholder.typicode.com/users"
            );
            const users = response.data;  //dados
            res
            .status(200)
            .json({
                message:
                "Aqui estão os usuários captados da API pública JSONPlaceholder",
                 users,
                });
        }catch(error){
            console.error(error);
            res.status(500).json({error:"Falha ao encontrar usuários"});
        }
    }

    static async getUsersWebsiteIO(req,res){
        try {
            const response = await axios.get(
                "https://jsonplaceholder.typicode.com/users"
            );

            const users = response.data.filter(
                (user) => user.website.endsWith(".io")
            )

            res
            .status(200)
            .json({
                message:
                "AQUI ESTÃO OS USERS COM DOMINIO IO",
                 users,
                });

        }catch(error){
            console.log(error)

            res.status(500).json({error:"DEU RUIM", error})
        }
    }

    static async getUsersWebsiteNET(req,res){
        try {
            const response = await axios.get(
                "https://jsonplaceholder.typicode.com/users"
            );

            const netUsers = response.data.filter(
                (user) => user.website.endsWith(".net")
            )

            res.status(200).json({
                message: "AQUI ESTÃO OS USERS COM DOMÍNIO NET",
                netUsers,
            });

        }catch(error){
            console.log(error)
            res.status(500).json({error:"DEU RUIM", error})
        }
    }

    static async getUsersWebsiteCOM(req,res){
        try {
            const response = await axios.get(
                "https://jsonplaceholder.typicode.com/users"
            );

            const comUsers = response.data.filter(
                (user) => user.website.endsWith(".com")
            )

            res.status(200).json({
                message: "AQUI ESTÃO OS USERS COM DOMÍNIO COM",
                comUsers,
            });

        }catch(error){
            console.log(error)
            res.status(500).json({error:"DEU RUIM", error})
        }
    }
};