const router = require('express').Router();
const alunoController = require('../controller/alunoController');
const teacherController = require('../controller/teacherController');
const JSONPlaceholderController = require('../controller/JSONPlaceholderController');
//LEMBRE DO IMPORT

router.put('/atualizacaoAluno/', alunoController.updateAluno);
router.delete('/deleteAluno/:id', alunoController.deleteAluno);

router.get('/teacher/', teacherController.getTeacher);
router.post('/aluno/', alunoController. postAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getUsersWebsiteIO);
router.get("/external/net", JSONPlaceholderController.getUsersWebsiteNET)
router.get("/external/com", JSONPlaceholderController.getUsersWebsiteCOM);

module.exports = router;